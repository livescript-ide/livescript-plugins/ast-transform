This is base type(I don't use classe) for implementing livescript ast transform plugins.

It implements only two methods `install` and `unistall` and requires user to implement method `enable` and `disable`


# How to use

```livescript
require! <[ livescript-ast-transform ]>
SuperAstPlugin = Object.create livescript-ast-transform
  ..enable = !->
    # you can access livescript module through @livescript
    # also here you should save original version of every property you will modify
    # so you can restore it later
  ..disable = !->
    # here you should restore modification you made to the compiler
```

Because livescript lack any plugin infrastructure for now this module assumes that it is the only one that can modify ast.

# License
[BSD-3-Clause](License.md)
