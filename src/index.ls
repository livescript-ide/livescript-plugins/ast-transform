module.exports =
    # TODO: test disable
    disable: !->
        throw new Error "Plugins for now must manualy implement disable method."

    # TODO: test enable
    enable: !->
        throw new Error "Plugins must implement enable method."

    install: (livescript,lexer) ->
        unless @name
            throw new Error "Transform need to have a name"
        try
            livescript ?= require \livescript
            lexer ?= require \livescript/lib/lexer
        unless livescript
            throw new Error "Missing Livescript Compiler in @livescript/#{@name}"
        unless lexer
            throw new Error "Missing Livescript Lexer in @livescript/#{@name}"
        
        @livescript = livescript
        @livescript.lexer = lexer
        @enable!

        livescript.ast[]plugins.push @

    # TODO: test uninstall
    uninstall: !->
        @disable!
        @livescript.ast[]plugins
            idx = index-of @
            ..splice idx, 1
